All texts in german  
~

Dies sind privat erstellte Lernzettel zum Kurs "Diskrete Strukturen", welche ich im ersten Semester meines Informatikstudiums erstellt habe.

Die Materialien in dieser Repository können nach CC-BY-4.0 frei verwendet und repliziert werden.  


Ostfalia - Hochschule für angewandte Wissenschaft  
Diskrete Strukturen SoSe 2017  
Kursnummer #2011
